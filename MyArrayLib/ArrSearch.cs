﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    public class ArrSearch
    {
        static int BinarySearch(int[] array, int left, int right, int key)
        {
            if (left > right)
            {
                return -1;
            }
            int middle = (left + right) / 2;
            if (array[middle] == key)
            {
                while (middle - 1 > 0 && array[middle - 1] == key)
                {
                    middle--;
                }
                return middle;
            }
            if (key > array[middle])
            {
                return BinarySearch(array, middle + 1, right, key);
            }
            else
            {
                return BinarySearch(array, left, middle - 1, key);
            }
        }

        public static int BinarySearch(int[] array, int key)
        {
            return BinarySearch(array, 0, array.Length - 1, key);
        }
    }
}
