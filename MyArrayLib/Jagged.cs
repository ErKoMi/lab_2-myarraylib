﻿using System;

namespace lab2
{
    /// <summary>
    /// Класс для работы со ступенчатыми массивами
    /// </summary>
    public class Jagged
    {
        /// <summary>
        /// выводит ступенчатый массив на экран
        /// </summary>
        public static void Print(int[][] jd)
        {
            try
            {
                for (int i = 0; i < jd.Length; i++)
                {
                    for (int j = 0; j < jd[i].Length; j++)
                    {
                        Console.Write("{0}\t", jd[i][j]);
                    }
                    Console.WriteLine();
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("[]");
            }
        }
        /// <summary>
        /// выводит ступенчатый массив на экран
        /// </summary>
        public static void Print(double[][] jd)
        {
            try
            {
                for (int i = 0; i < jd.Length; i++)
                {
                    for (int j = 0; j < jd[i].Length; j++)
                    {
                        Console.Write("{0:f}\t", jd[i][j]);
                    }
                    Console.WriteLine();
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("[]");
            }
        }
        /// <summary>
        /// выводит ступенчатый массив на экран
        /// </summary>
        public static void Print(long[][] jd)
        {
            try
            {
                for (int i = 0; i < jd.Length; i++)
                {
                    for (int j = 0; j < jd[i].Length; j++)
                    {
                        Console.Write("{0}\t", jd[i][j]);
                    }
                    Console.WriteLine();
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("[]");
            }
        }
    }
}
