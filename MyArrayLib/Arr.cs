﻿using System;

namespace lab2
{
    /// <summary>
    /// Класс для работы с массивами
    /// </summary>
    public class Arr
    {
        /// <summary>
        /// выводит массив на экран
        /// </summary>
        /// <param name="arr">массив</param>
        public static void PrintArr(int[] arr)
        {
            try
            {
                int N = arr.Length;
                Console.Write("[");
                for (int i = 0; i < (N - 1); i++)
                {
                    Console.Write("{0}, ", arr[i]);
                }
                Console.WriteLine(arr[N - 1] + "]");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("]");
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка формата!");
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("null");
            }
            catch (System.NullReferenceException)
            {
                Console.WriteLine("null");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// выводит массив на экран
        /// </summary>
        /// <param name="arr">массив</param>
        public static void PrintArr(double[] arr)
        {
            try
            {
                Console.Write("[");
                for (int i = 0; i < (arr.Length - 1); i++)
                {
                    Console.Write("{0:f}, ", arr[i]);
                }
                Console.WriteLine("{0:f}" + "]", arr[arr.Length - 1]);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("]");
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка формата!");
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("null");
            }
            catch (System.NullReferenceException)
            {
                Console.WriteLine("null");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// выводит массив на экран
        /// </summary>
        /// <param name="arr">массив</param>
        public static void PrintArr(long[] arr)
        {
            try
            {
                int N = arr.Length;
                Console.Write("[");
                for (int i = 0; i < (arr.Length - 1); i++)
                {
                    Console.Write("{0}, ", arr[i]);
                }
                Console.WriteLine(arr[arr.Length - 1] + "]");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("]");
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка формата!");
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("null");
            }
            catch (System.NullReferenceException)
            {
                Console.WriteLine("null");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        /// <summary>
        /// выводит массив на экран
        /// </summary>
        /// <param name="arr">массив</param>
        public static void PrintArr(string[] arr)
        {
            try
            {
                Console.Write("[");
                for (int i = 0; i < (arr.Length - 1); i++)
                {
                    Console.Write("\"{0}\", ", arr[i]);
                }
                Console.WriteLine("\"" + arr[arr.Length - 1] + "\"");
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("]");
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка формата!");
            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("null");
            }
            catch (System.NullReferenceException)
            {
                Console.WriteLine("null");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// создает массив длины N и заполняет случайными целыми значениями из диапазона (a,b) по умолчанию (0,100)
        /// </summary>
        /// <param name="N">длина массива</param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>массив</returns>
        public static int[] RandomArr(int N, int a = 0, int b = 100)
        {
            if (a > b)
                (a, b) = (b, a);
            if (N < 1)
            {
                throw new Exception("Длинна массива должна быть не меньше 1!");
            }
            int[] arr = new int[N];
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(a, b);
            }
            return arr;
        }
        /// <summary>
        /// создает массив длины N и заполняет случайными дробными значениями из диапазона (a,b) по умолчанию (0,100)
        /// </summary>
        /// <param name="N">длина массива</param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>массив</returns>
        public static double[] RandomDoubleArr(int N, int a = 0, int b = 100)
        {
            if (a > b)
                (a, b) = (b, a);
            if (N < 1)
            {
                throw new Exception("Длинна массива должна быть не меньше 1!");
            }
            double[] arr = new double[N];
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(a, b) + rnd.NextDouble();
            }
            return arr;
        }

        /// <summary>
        /// создает массив размера N и заполняет пользовательскими значениями
        /// Элементы вводятся через пробел в одну строчку
        /// </summary>
        /// <param name="N">число элементов</param>
        /// <returns>матрицу</returns>
        public static int[] UserArr(int N)
        {
            if (N < 1)
            {
                throw new Exception("Размер массива не может быть меньше чем 1!");
            }
            int[] arr = new int[N];
            bool f = true;
            do
            {
                string str = Console.ReadLine();
                try
                {
                    for (int i = 0; i < N; i++)
                    {
                        arr[i] = int.Parse(str.Split()[i]);
                        f = true;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Неверное количество элементов! Повторитте ввод строки!");
                    f = false;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Некорректный ввод! Введите строку заново!");
                    f = false;
                }
                catch (ArgumentNullException)
                {
                    Console.WriteLine("Некорректный ввод! Введите строку заново!");
                    f = false;
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Слишком большое число для типа int! Введите строку заново!");
                    f = false;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (!f);
            return arr;
        }
        /// <summary>
        /// создает массив размера N и заполняет пользовательскими значениями типа double
        /// Элементы вводятся через пробел в одну строчку, разделитель целой и дробной частей - запятая
        /// </summary>
        /// <param name="N">число элементов</param>
        /// <returns>матрицу</returns>
        public static double[] UserDoubleArr(int N)
        {
            if (N < 1)
            {
                throw new Exception("Размер массива не может быть меньше чем 1!");
            }
            double[] arr = new double[N];
            bool f = true;
            do
            {
                string str = Console.ReadLine();
                try
                {
                    for (int i = 0; i < N; i++)
                    {
                        arr[i] = double.Parse(str.Split()[i]);
                        f = true;
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Неверное количество элементов! Повторитте ввод строки!");
                    f = false;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Некорректный ввод! Введите строку заново!");
                    f = false;
                }
                catch (ArgumentNullException)
                {
                    Console.WriteLine("Некорректный ввод! Введите строку заново!");
                    f = false;
                }
                catch (OverflowException)
                {
                    Console.WriteLine("Слишком большое число для типа double! Введите строку заново!");
                    f = false;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (!f);
            return arr;
        }

        /// <summary>
        /// Ищет максимальный элемент массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>максимальный элемент</returns>
        public static int Max(int[] arr)
        {
            int max = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }
            }
            return max;
        }
        /// <summary>
        /// Ищет максимальный элемент массива и записывает в переменную с параметром out индекс этого элемента
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="index">индекс</param>
        /// <returns>максимальный элемент</returns>
        public static int Max(int[] arr, out int index)
        {
            int max = arr[0], ix = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    ix = i;
                }
            }
            index = ix;
            return max;
        }
        /// <summary>
        /// Ищет максимальный элемент массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>максимальный элемент</returns>
        public static double Max(double[] arr)
        {
            double max = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }
            }
            return max;
        }
        /// <summary>
        /// Ищет максимальный элемент массива и записывает в переменную с параметром out индекс этого элемента
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="index">индекс</param>
        /// <returns>максимальный элемент</returns>
        public static double Max(double[] arr, out int index)
        {
            double max = arr[0];
            int ix = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    ix = i;
                }
            }
            index = ix;
            return max;
        }
        /// <summary>
        /// Ищет максимальный элемент массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>максимальный элемент</returns>
        public static long Max(long[] arr)
        {
            long max = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                }
            }
            return max;
        }
        /// <summary>
        /// Ищет максимальный элемент массива и записывает в переменную с параметром out индекс этого элемента
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="index">индекс</param>
        /// <returns>максимальный элемент</returns>
        public static long Max(long[] arr, out int index)
        {
            long max = arr[0];
            int ix = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] > max)
                {
                    max = arr[i];
                    ix = i;
                }
            }
            index = ix;
            return max;
        }

        /// <summary>
        /// Ищет минимальный элемент массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>минимальный элемент</returns>
        public static int Min(int[] arr)
        {
            int min = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }
            }
            return min;
        }
        /// <summary>
        /// Ищет минимальный элемент массива и
        /// записывает в переменную с параметром out индекс первого вхождения этого элемента
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="index">индекс</param>
        /// <returns>минимальный элемент</returns>
        public static int Min(int[] arr, out int index)
        {
            int min = arr[0], ix = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    ix = i;
                }
            }
            index = ix;
            return min;
        }
        /// <summary>
        /// Ищет минимальный элемент массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>минимальный элемент</returns>
        public static double Min(double[] arr)
        {
            double min = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }
            }
            return min;
        }
        /// <summary>
        /// Ищет минимальный элемент массива и
        /// записывает в переменную с параметром out индекс первого вхождения этого элемента
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="index">индекс</param>
        /// <returns>минимальный элемент</returns>
        public static double Min(double[] arr, out int index)
        {
            double min = arr[0];
            int ix  = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    ix = i;
                }
            }
            index = ix;
            return min;
        }
        /// <summary>
        /// Ищет минимальный элемент массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>минимальный элемент</returns>
        public static long Min(long[] arr)
        {
            long min = arr[0];
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }
            }
            return min;
        }
        /// <summary>
        /// Ищет минимальный элемент массива и
        /// записывает в переменную с параметром out индекс первого вхождения этого элемента
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="index">индекс</param>
        /// <returns>минимальный элемент</returns>
        public static long Min(long[] arr, out int index)
        {
            long min = arr[0];
            int ix = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    ix = i;
                }
            }
            index = ix;
            return min;
        }


        /// <summary>
        /// Считает смму элементов массива в интервале (K, L)
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="K">нижняя граница интервала</param>
        /// <param name="L">верхняя граница интервала</param>
        /// <returns>сумма элементов в интервале</returns>
        public static long Sum(int[] arr, int K, int L)
        {
            if (K > L)
            {
                int temp = L;
                L = K;
                K = temp;
            }
            try
            {
                long sum = 0;
                for (int i = K; i <= L; i++)
                {
                    checked
                    {
                        sum += arr[i];
                    }
                }
                return sum;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }
        /// <summary>
        /// Считает смму элементов массива в интервале (K, L)
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="K">нижняя граница интервала</param>
        /// <param name="L">верхняя граница интервала</param>
        /// <returns>сумма элементов в интервале</returns>
        public static double Sum(double[] arr, int K, int L)
        {
            if (K > L)
            {
                int temp = L;
                L = K;
                K = temp;
            }
            try
            {
                double sum = 0;
                for (int i = K; i <= L; i++)
                {
                    checked
                    {
                        sum += arr[i];
                    }
                }
                return sum;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }
        /// <summary>
        /// Сумма элементов массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>сумма</returns>
        public static long Sum(int[] arr)
        {
            try
            {
                long sum = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    checked
                    {
                        sum += arr[i];
                    }
                }
                return sum;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }
        /// <summary>
        /// Сумма элементов массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns>сумма</returns>
        public static double Sum(double[] arr)
        {
            try
            {
                double sum = 0;
                for (int i = 0; i < arr.Length; i++)
                {
                    checked
                    {
                        sum += arr[i];
                    }
                }
                return sum;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }

        /// <summary>
        /// Считает произвдение элементов массива в интервале (K, L)
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="K">нижняя граница интервала</param>
        /// <param name="L">верхняя граница интервала</param>
        /// <returns>Произведение элементов в интервале</returns>
        public static long Com(int[] arr, int K, int L)
        {
            if (K > L)
            {
                int temp = L;
                L = K;
                K = temp;
            }
            try
            {
                long com = 1;
                for (int i = K; i <= L; i++)
                {
                    checked
                    {
                        com *= arr[i];
                    }
                }
                return com;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }
        /// <summary>
        /// Считает произвдение элементов массива в интервале (K, L)
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="K">нижняя граница интервала</param>
        /// <param name="L">верхняя граница интервала</param>
        /// <returns>Произведение элементов в интервале</returns>
        public static double Com(double[] arr, int K, int L)
        {
            if (K > L)
            {
                int temp = L;
                L = K;
                K = temp;
            }
            try
            {
                double com = 1;
                for (int i = K; i <= L; i++)
                {
                    checked
                    {
                        com *= arr[i];
                    }
                }
                return com;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }
        /// <summary>
        /// Считаем произведение элементов массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns></returns>
        public static long Com(int[] arr)
        {
            try
            {
                long com = 1;
                for (int i = 0; i < arr.Length; i++)
                {
                    checked
                    {
                        com *= arr[i];
                    }
                }
                return com;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }
        /// <summary>
        /// Считаем произведение элементов массива
        /// </summary>
        /// <param name="arr">массив</param>
        /// <returns></returns>
        public static double Com(double[] arr)
        {
            try
            {
                double com = 1;
                for (int i = 0; i < arr.Length; i++)
                {
                    checked
                    {
                        com *= arr[i];
                    }
                }
                return com;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return long.MinValue;
        }

        /// <summary>
        /// ищет элементы равные el
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="el">искомый элементы</param>
        /// <returns>массив индексов</returns>
        public static int[] Indexes(int[] arr, int el)
        {
            int[] inxs = new int[0];
            int c = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == el)
                {
                    Array.Resize(ref inxs, c + 1);
                    inxs[c] = i;
                    c++;
                }
            }
            return inxs;
        }
        /// <summary>
        /// ищет элементы равные el
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="el">искомый элементы</param>
        /// <returns>массив индексов</returns>
        public static int[] Indexes(long[] arr, long el)
        {
            int[] inxs = new int[0];
            int c = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == el)
                {
                    Array.Resize(ref inxs, c + 1);
                    inxs[c] = i;
                    c++;
                }
            }
            return inxs;
        }
        /// <summary>
        /// ищет элементы равные el
        /// </summary>
        /// <param name="arr">массив</param>
        /// <param name="el">искомый элементы</param>
        /// <returns>массив индексов</returns>
        public static int[] Indexes(double[] arr, double el)
        {
            int[] inxs = new int[0];
            int c = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == el)
                {
                    Array.Resize(ref inxs, c + 1);
                    inxs[c] = i;
                    c++;
                }
            }
            return inxs;
        }
    }
}
