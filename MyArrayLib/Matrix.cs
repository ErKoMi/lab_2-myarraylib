﻿using System;

namespace lab2
{
    /// <summary>
    /// Класс для рабоыты с матрицами
    /// </summary>
    public class Matrix
    {
        /// <summary>
        /// создает квадратную матрицу размера N и заполняет случайными целыми положительными значениями из диапазона a, b (по умолчанию - (0,100))
        /// </summary>
        /// <param name="N">размер матрицы</param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>матрицу</returns>
        public static int[,] RandomMatrix(int N, int a = 0, int b = 100)
        {
            if (a > b)
                (a, b) = (b, a);
            if (N < 1)
            {
                throw new Exception("Длинна массива должна быть не меньше 1x1!");
            }
            int[,] arr = new int[N, N];
            Random rnd = new Random();
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = rnd.Next(a, b);
                }
            }
            return arr;
        }
        /// <summary>
        /// создает квадратную матрицу размера N и заполняет случайными дробными значениями
        /// из диапазона a, b (по умолчанию - (0,100))
        /// </summary>
        /// <param name="N">размер матрицы</param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>матрицу</returns>
        public static double[,] RandomDoubleMatrix(int N, int a = 0, int b = 100)
        {
            if (a > b)
                (a, b) = (b, a);
            if (N < 1)
            {
                throw new Exception("Размер матрицы должен быть не меньше 1х1!");
            }
            double[,] matrix = new double[N, N];
            Random rnd = new Random();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rnd.Next(a, b) + rnd.NextDouble();
                }
            }
            return matrix;
        }
        /// <summary>
        /// создает квадратную матрицу размера N*M и заполняет случайными целыми значениями из диапазона a, b (по умолчанию - (0,100))
        /// </summary>
        /// <param name="N">число строк</param>
        /// <param name="M">число столбцов</param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>матрицу</returns>
        public static int[,] RandomMatrix(int N, int M, int a = 0, int b = 100)
        {
            if (a > b)
                (a, b) = (b, a);
            if (N < 1 || M < 1)
            {
                throw new Exception("Размер матрицы не может быть меньше чем 1х1!");
            }
            int[,] arr = new int[N, M];
            Random rnd = new Random();
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = rnd.Next(a, b);
                }
            }
            return arr;
        }
        /// <summary>
        /// создает квадратную матрицу размера N*M и заполняет случайными дробными значениями
        /// из диапазона a, b (по умолчанию - (0,100))
        /// </summary>
        /// <param name="N">число строк</param>
        /// <param name="M">число столбцов</param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>матрицу</returns>
        public static double[,] RandomDoubleMatrix(int N, int M, int a = 0, int b = 100)
        {
            if (a > b)
                (a, b) = (b, a);
            if (N < 1 || M < 1)
            {
                throw new Exception("Размер матрицы не может быть меньше чем 1х1!");
            }
            double[,] arr = new double[N, M];
            Random rnd = new Random();
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = rnd.NextDouble() + rnd.Next(a, b);
                }
            }
            return arr;
        }

        /// <summary>
        /// создает квадратную матрицу размера N и заполняет значениями
        /// </summary>
        /// <param name="N">размер матрицы</param>
        /// <returns>матрицу</returns>
        public static int[,] UserMatrix(int N)
        {
            if (N < 1)
            {
                throw new Exception("Длинна массива должна быть не меньше 1!");
            }
            int[,] matrix = new int[N, N];
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                do
                {
                    string str = Console.ReadLine();
                    try
                    {
                        for (int j = 0; j < N; j++)
                        {
                            matrix[i, j] = int.Parse(str.Split()[j]);
                            f = true;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine("Недостаточно элементов! Повторитте ввод строки!");
                        f = false;
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (ArgumentNullException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Слишком большое число для типа int! Введите строку заново!");
                        f = false;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (!f);
            }
            return matrix;
        }
        /// <summary>
        /// создает квадратную матрицу размера N и заполняет значениями типа double
        /// </summary>
        /// <param name="N">размер матрицы</param>
        /// <returns>матрицу</returns>
        public static double[,] UserDoubleMatrix(int N)
        {
            if (N < 1)
            {
                throw new Exception("Длинна массива должна быть не меньше 1!");
            }
            double[,] matrix = new double[N, N];
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                do
                {
                    string str = Console.ReadLine();
                    try
                    {
                        for (int j = 0; j < N; j++)
                        {
                            matrix[i, j] = double.Parse(str.Split()[j]);
                            f = true;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine("Недостаточно элементов! Повторитте ввод строки!");
                        f = false;
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (ArgumentNullException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Слишком большое число для типа int! Введите строку заново!");
                        f = false;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (!f);
            }
            return matrix;
        }
        /// <summary>
        /// создает матрицу размера N*M и заполняет пользовательскими значениями 
        /// </summary>
        /// <param name="N">число строк</param>
        /// <param name="M">число столбцов</param>
        /// <returns>матрицу</returns>
        public static int[,] UserMatrix(int N, int M)
        {
            if (N < 1 || M < 1)
            {
                throw new Exception("Размер матрицы не может быть меньше чем 1х1!");
            }
            int[,] matrix = new int[N, M];
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                do
                {
                    string str = Console.ReadLine();
                    try
                    {
                        for (int j = 0; j < M; j++)
                        {
                            matrix[i, j] = int.Parse(str.Split()[j]);
                            f = true;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine("Недостаточно элементов! Повторитте ввод строки!");
                        f = false;
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (ArgumentNullException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Слишком большое число для типа int! Введите строку заново!");
                        f = false;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (!f);
            }
            return matrix;
        }
        /// <summary>
        /// создает матрицу размера N*M и заполняет пользовательскими значениями типа double
        /// </summary>
        /// <param name="N">число строк</param>
        /// <param name="M">число столбцов</param>
        /// <returns>матрицу</returns>
        public static double[,] UserDoubleMatrix(int N, int M)
        {
            if (N < 1 || M < 1)
            {
                throw new Exception("Размер матрицы не может быть меньше чем 1х1!");
            }
            double[,] matrix = new double[N, M];
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                do
                {
                    string str = Console.ReadLine();
                    try
                    {
                        for (int j = 0; j < M; j++)
                        {
                            matrix[i, j] = double.Parse(str.Split()[j]);
                            f = true;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        Console.WriteLine("Недостаточно элементов! Повторитте ввод строки!");
                        f = false;
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (ArgumentNullException)
                    {
                        Console.WriteLine("Некорректный ввод! Введите строку заново!");
                        f = false;
                    }
                    catch (OverflowException)
                    {
                        Console.WriteLine("Слишком большое число для типа int! Введите строку заново!");
                        f = false;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (!f);
            }
            return matrix;
        }

        /// <summary>
        /// выводит матрицу на экран
        /// </summary>
        /// <param name="matrix">массив</param>
        public static void Print(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            try
            {
                string bord = "---";
                for (int i = 0; i < matrix.GetLength(1) - 1; i++)
                {
                    bord = bord + "---------";
                }
                Console.WriteLine(bord);
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        Console.Write("{0}\t", matrix[i, j]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine(bord);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("[]");
            }
        }
        /// <summary>
        /// выводит матрицу на экран
        /// </summary>
        /// <param name="matrix">массив</param>
        public static void Print(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            try
            {
                string bord = "---";
                for (int i = 0; i < matrix.GetLength(1) - 1; i++)
                {
                    bord = bord + "---------";
                }
                Console.WriteLine(bord);
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        Console.Write("{0:f}\t", matrix[i, j]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine(bord);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("[]");
            }
        }

        /// <summary>
        /// Выводит главную диагональ матрицы в массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>главная диагональ</returns>
        public static int[] MainDiagonal(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int[] diag = new int[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                diag[i] = matrix[i, i];
            }
            return diag;
        }
        /// <summary>
        /// Выводит главную диагональ матрицы в массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>главная диагональ</returns>
        public static double[] MainDiagonal(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            double[] diag = new double[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                diag[i] = matrix[i, i];
            }
            return diag;
        }
        /// <summary>
        /// Возвращает массив диагоналей параллельных главной
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>ступенчатый массивов</returns>
        public static int[][] MDiagonals(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int c = 0;
            int N = matrix.GetLength(0);
            int[][] diag = new int[2 * N - 1][];
            for (int k = 0; k < N; k++)
            {
                diag[c] = new int[k + 1];
                for (int i = 0; i <= k; i++)
                {
                    diag[c][i] = matrix[N - 1 - k + i, i];
                }
                c++;
            }
            for (int k = N - 2; k >= 0; k--)
            {
                diag[c] = new int[k + 1];
                for (int i = 0; i <= k; i++)
                {
                    diag[c][i] = matrix[i, N - 1 - k + i];
                }
                c++;
            }

            return diag;
        }
        /// <summary>
        /// Возвращает массив диагоналей параллельных главной
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>ступенчатый массивов</returns>
        public static double[][] MDiagonals(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int c = 0;
            int N = matrix.GetLength(0);
            double[][] diag = new double[2 * N - 1][];
            for (int k = 0; k < N; k++)
            {
                diag[c] = new double[k + 1];
                for (int i = 0; i <= k; i++)
                {
                    diag[c][i] = matrix[N - 1 - k + i, i];
                }
                c++;
            }
            for (int k = N - 2; k >= 0; k--)
            {
                diag[c] = new double[k + 1];
                for (int i = 0; i <= k; i++)
                {
                    diag[c][i] = matrix[i, N - 1 - k + i];
                }
                c++;
            }

            return diag;
        }

        /// <summary>
        /// Выводит побочную диагональ матрицы в массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>главная диагональ</returns>
        public static int[] SideDiagonal(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int N = matrix.GetLength(0);
            int[] diag = new int[N];
            for (int i = 0; i < N; i++)
            {
                diag[i] = matrix[i, N - 1 - i];
            }
            return diag;
        }
        /// <summary>
        /// Выводит побочную диагональ матрицы в массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>главная диагональ</returns>
        public static double[] SideDiagonal(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int N = matrix.GetLength(0);
            double[] diag = new double[N];
            for (int i = 0; i < N; i++)
            {
                diag[i] = matrix[i, N - 1 - i];
            }
            return diag;
        }

        /// <summary>
        /// Возвращает массив диагоналей параллельных побочной
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>ступенчатый массивов</returns>
        public static int[][] SDiagonals(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int c = 0;
            int N = matrix.GetLength(0);
            int[][] diag = new int[2 * N - 1][];
            for (int k = 1; k <= N; k++)
            {
                diag[c] = new int[k];
                for (int i = 0; i < k; i++)
                {
                    diag[c][i] = matrix[i, k - 1 - i];
                }
                c++;
            }
            for (int k = N - 1; k > 0; k--)
            {
                diag[c] = new int[k];
                for (int i = 0; i < k; i++)
                {
                    diag[c][k - 1 - i] = matrix[N - 1 - i, N - k + i]; 
                }
                c++;
            }
            return diag;
        }
        /// <summary>
        /// Возвращает массив диагоналей параллельных побочной
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>ступенчатый массивов</returns>
        public static double[][] SDiagonals(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int c = 0;
            int N = matrix.GetLength(0);
            double[][] diag = new double[2 * N - 1][];
            for (int k = 1; k <= N; k++)
            {
                diag[c] = new double[k];
                for (int i = 0; i < k; i++)
                {
                    diag[c][i] = matrix[i, k - 1 - i];
                }
                c++;
            }
            for (int k = N - 1; k > 0; k--)
            {
                diag[c] = new double[k];
                for (int i = 0; i < k; i++)
                {
                    diag[c][k - 1 - i] = matrix[N - 1 - i, N - k + i];
                }
                c++;
            }
            return diag;
        }

        /// <summary>
        /// Возвращает строку матрицы 
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="n">номер строки (с 0)</param>
        /// <returns>строку матрицы</returns>
        public static int[] GetStr(int[,] matrix, int n)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (n > matrix.GetLength(0) - 1)
            {
                throw new Exception("Строки с таким номером не существует!");
            }
            int[] str = new int[matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                str[i] = matrix[n, i];
            }
            return str;
        }
        /// <summary>
        /// Возвращает строку матрицы 
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="n">номер строки (с 0)</param>
        /// <returns>строку матрицы</returns>
        public static double[] GetStr(double[,] matrix, int n)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (n > matrix.GetLength(0) - 1)
            {
                throw new Exception("Строки с таким номером не существует!");
            }
            double[] str = new double[matrix.GetLength(1)];
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                str[i] = matrix[n, i];
            }
            return str;
        }

        /// <summary>
        /// Возвращает столбец матрицы 
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="n">номер столбца (с 0)</param>
        /// <returns>столбец матрицы</returns>
        public static int[] GetCol(int[,] matrix, int n)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (n > matrix.GetLength(1) - 1)
            {
                throw new Exception("Столбца с таким номером не существует!");
            }
            int[] column = new int[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                column[i] = matrix[i, n];
            }
            return column;
        }
        /// <summary>
        /// Возвращает столбец матрицы 
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="n">номер столбца (с 0)</param>
        /// <returns>столбец матрицы</returns>
        public static double[] GetDoubleCol(int[,] matrix, int n)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            if (n > matrix.GetLength(1) - 1)
            {
                throw new Exception("Столбца с таким номером не существует!");
            }
            double[] column = new double[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                column[i] = matrix[i, n];
            }
            return column;
        }

        /// <summary>
        /// Номера строк где все элементы четны, если такой строки нет - вернет пустой массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив номеров</returns>
        public static int[] IsHonest(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            int N = matrix.GetLength(0);
            int[] result = new int[0];
            int c = 0;
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                foreach (int a in Matrix.GetStr(matrix, i))
                {
                    if (a % 2 != 0)
                    {
                        f = false;
                        break;
                    }
                }
                if (f)
                {
                    c++;
                    Array.Resize(ref result, c);
                    result[c - 1] = i;
                }
            }

            return result;

        }
        /// <summary>
        /// Номера строк где все элементы четны, если такой строки нет - вернет пустой массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив номеров типа long</returns>
        public static long[] IsHonestL(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            int N = matrix.GetLength(0);
            long[] result = new long[0];
            int c = 0;
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                foreach (int a in Matrix.GetStr(matrix, i))
                {
                    if (a % 2 != 0)
                    {
                        f = false;
                        break;
                    }
                }
                if (f)
                {
                    c++;
                    Array.Resize(ref result, c);
                    result[c - 1] = i;
                }
            }

            return result;

        }
        /// <summary>
        /// Номера строк где все элементы четны, если такой строки нет - вернет пустой массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив номеров</returns>
        public static int[] IsHonest(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            int N = matrix.GetLength(0);
            int[] result = new int[0];
            int c = 0;
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                foreach (int a in Matrix.GetStr(matrix, i))
                {
                    if (a % 2 != 0)
                    {
                        f = false;
                        break;
                    }
                }
                if (f)
                {
                    c++;
                    Array.Resize(ref result, c);
                    result[c - 1] = i;
                }
            }

            return result;

        }
        /// <summary>
        /// Номера строк где все элементы четны, если такой строки нет - вернет пустой массив
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив номеров типа long</returns>
        public static long[] IsHonestL(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            int N = matrix.GetLength(0);
            long[] result = new long[0];
            int c = 0;
            for (int i = 0; i < N; i++)
            {
                bool f = true;
                foreach (int a in Matrix.GetStr(matrix, i))
                {
                    if (a % 2 != 0)
                    {
                        f = false;
                        break;
                    }
                }
                if (f)
                {
                    c++;
                    Array.Resize(ref result, c);
                    result[c - 1] = i;
                }
            }

            return result;

        }

        /// <summary>
        /// Ищет максимальный элемент в матрице
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>максимальный элемент</returns>
        public static int Max(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int mx = matrix[0, 0];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] > mx)
                    {
                        mx = matrix[i, j];
                    }
                }
            }
            return mx;
        }
        /// <summary>
        /// Ищет максимальный элемент в матрице
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>максимальный элемент</returns>
        public static double Max(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            double mx = matrix[0, 0];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] > mx)
                    {
                        mx = matrix[i, j];
                    }
                }
            }
            return mx;
        }
        /// <summary>
        /// Ищет максимальный элемент в матрице
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="index">индекс первого вхождения максимального элемента</param>
        /// <returns>максимальный элемент</returns>
        public static int Max(int[,] matrix, out int[] index)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int mx = matrix[0, 0];
            index = new int[] { 0, 0 };
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] > mx)
                    {
                        mx = matrix[i, j];
                        index[0] = i;
                        index[1] = j;
                    }
                }
            }
            return mx;
        }
        /// <summary>
        /// Ищет максимальный элемент в матрице
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="index">индекс первого вхождения элемента</param>
        /// <returns>максимальный элемент</returns>
        public static double Max(double[,] matrix, out int[] index)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            double mx = matrix[0, 0];
            index = new int[] { 0, 0 };
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] > mx)
                    {
                        mx = matrix[i, j];
                        index[0] = i;
                        index[1] = j;
                    }
                }
            }
            return mx;
        }

        /// <summary>
        /// Ищет минимальный элемент в матрице
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="index"></param>
        /// <returns>минимальный элемент</returns>
        /// 
        public static int Min(int[,] matrix, out int[] index)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            int mn = matrix[0, 0];
            index = new int[] { 0, 0 };
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < mn)
                    {
                        mn = matrix[i, j];
                        index[0] = i;
                        index[1] = j;
                    }
                }
            }
            return mn;
        }
        /// <summary>
        /// Ищет минимальный элемент в матрице
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <param name="index">индекс первого вхождения элемента</param>
        /// <returns>минимальный элемент</returns>
        /// 
        public static double Min(double[,] matrix, out int[] index)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            index = new int[] { 0, 0 };
            double mn = matrix[0, 0];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < mn)
                    {
                        mn = matrix[i, j];
                        index[0] = i;
                        index[1] = j;
                    }
                }
            }
            return mn;
        }

        /// <summary>
        /// Считает сумму строк матрицы
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив сумм</returns>
        public static long[] Sum(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            try
            {
                long[] sum = new long[matrix.GetLength(0)];
                for (int i = 0; i <= matrix.GetLength(0); i++)
                {
                    long s = 0;
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        checked
                        {
                            s += matrix[i, j];
                        }
                    }
                    sum[i] = s;
                }
                return sum;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return new long[] { long.MinValue };
        }
        /// <summary>
        /// Считает сумму строк матрицы
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив сумм</returns>
        public static double[] Sum(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            try
            {
                double[] sum = new double[matrix.GetLength(0)];
                for (int i = 0; i <= matrix.GetLength(0); i++)
                {
                    double s = 0;
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        checked
                        {
                            s += matrix[i, j];
                        }
                    }
                    sum[i] = s;
                }
                return sum;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return new double[] { double.MinValue};
        }

        /// <summary>
        /// Считает произведение строк матрицы
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив сумм</returns>
        public static long[] StrCom(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            try
            {
                long[] com = new long[matrix.GetLength(0)];
                for (int i = 0; i < matrix.GetLength(0); i++)
                {
                    long c = 1;
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        checked
                        {
                            c *= matrix[i, j];
                        }
                    }
                    com[i] = c;
                }
                return com;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return new long[] { long.MinValue };
        }
        /// <summary>
        /// Считает сумму строк матрицы
        /// </summary>
        /// <param name="matrix">матрица</param>
        /// <returns>массив сумм</returns>
        public static double[] StrCom(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            try
            {
                double[] com = new double[matrix.GetLength(0)];
                for (int i = 0; i <= matrix.GetLength(0); i++)
                {
                    double c = 1.0;
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        checked
                        {
                            c *= matrix[i, j];
                        }
                    }
                    com[i] = c;
                }
                return com;

            }
            catch (OverflowException)
            {
                Console.WriteLine("Переполнение!");
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return new double[] { double.MinValue };
        }

        /// <summary>
        /// Сумма максимального и минимального элемента каждой строки матрицы
        /// </summary>
        /// <returns>массив сумм</returns>
        public static long[] SumMinMax(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            long[] result = new long[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                result[i] = Arr.Max(Matrix.GetStr(matrix, i)) + Arr.Min(Matrix.GetStr(matrix, i));
            }
            return result;
        }
        /// <summary>
        /// Сумма максимального и минимального элемента каждой строки матрицы
        /// </summary>
        /// <returns>массив сумм</returns>
        public static double[] SumMinMax(double[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }

            double[] result = new double[matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                result[i] = Arr.Max(Matrix.GetStr(matrix, i)) + Arr.Min(Matrix.GetStr(matrix, i));
            }
            return result;
        }

        /// <summary>
        /// каждый элемент которого равен сумме элементов, расположенных за 
        /// первым отрицательным элементом каждого столбца исходного массива и 100,
        /// если все элементы столбца неотрицательн
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns></returns>
        public static long[] SumColAfterNeg(int[,] matrix)
        {
            if (matrix is null)
            {
                throw new ArgumentNullException(nameof(matrix));
            }
            int N = matrix.GetLength(0);
            long[] result = new long[N];
            for (int i = 0; i < N; i++)
            {
                bool f = false;
                long s = 100;
                foreach (int a in Matrix.GetCol(matrix, i))
                {
                    if (f)
                    {
                        s += a;
                    }
                    if (!f && a < 0)
                    {
                        f = true;
                        s = 0;
                    }
                }
                result[i] = s;
            }
            return result;
        }
    }
}
