﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab2
{
    /// <summary>
    /// Класс для сортировки массива
    /// </summary>
    public class ArrSort
    {
        /// <summary>
        /// Сортировка методом вставки
        /// </summary>
        /// <param name="arr"></param>
        public static void InsertSort(int[] arr)
        {

            try
            {
                if(arr.Length == 1)
                {
                    return;
                }
                for (int i = 1; i < arr.Length; i++)
                {
                    int key = arr[i];
                    int j = i - 1;
                    while (j >= 0 && arr[j] > key)
                    {
                        arr[j + 1] = arr[j];
                        j--;
                    }
                    arr[j + 1] = key;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Метод для слияния двух отсортировнных половин подмассива
        /// </summary>
        /// <param name="arr">целый массив</param>
        /// <param name="leftIndex">левый индекс подмассива</param>
        /// <param name="rightIndex">правый индекс подмассива</param>
        static void Merge(int[] arr, int leftIndex, int rightIndex)
        {
            int length = rightIndex - leftIndex + 1;
            int[] tmp = new int[length];
            int middleIndex = (rightIndex + leftIndex) / 2 + 1;
            int left = leftIndex, right = middleIndex, index = 0;

            while (left < middleIndex && right <= rightIndex)
            {
                if (arr[left] < arr[right])
                {
                    tmp[index] = arr[left];
                    left++;
                }
                else
                {
                    tmp[index] = arr[right];
                    right++;
                }
                index++;
            }

            while (left < middleIndex)
            {
                tmp[index] = arr[left];
                left++;
                index++;
            }
            while (right <= rightIndex)
            {
                tmp[index] = arr[right];
                right++;
                index++;
            }
            for (int i = 0; i < length; i++)
            {
                arr[leftIndex + i] = tmp[i];
            }

        }
        static void MergeSort(int[] arr, int leftIndex, int rightIndex)
        {

            if (leftIndex >= rightIndex)
            {
                return;
            }

            int middle = (rightIndex + leftIndex) / 2 + 1;

            MergeSort(arr, leftIndex, middle - 1);
            MergeSort(arr, middle, rightIndex);
            Merge(arr, leftIndex, rightIndex);

        }
        /// <summary>
        /// Cортировка методом слияния
        /// </summary>
        /// <param name="arr"></param>
        public static void MergeSort(int[] arr)
        {
            MergeSort(arr, 0, arr.Length - 1);
        }

        static int SetPivot(int[] array, int left, int right)
        {
            int pivot = array[right];
            int pivotIndex = left;
            for (int i = left; i < right; i++)
            {
                if (array[i] < pivot)
                {
                    int tmp = array[i];
                    array[i] = array[pivotIndex];
                    array[pivotIndex] = tmp;
                    pivotIndex++;
                }
            }
            array[right] = array[pivotIndex];
            array[pivotIndex] = pivot;
            return pivotIndex;
        }
        static void Quick(int[] array, int left, int right)
        {
            if (left >= right)
            {
                return;
            }
            int pivot = SetPivot(array, left, right);
            Quick(array, left, pivot - 1);
            Quick(array, pivot + 1, right);
        }

        /// <summary>
        /// Быстрая сортировка
        /// </summary>
        /// <param name="array"></param>
        public static void QuickSort(int[] array)
        {
            Quick(array, 0, array.Length - 1);
        }
    }
}
